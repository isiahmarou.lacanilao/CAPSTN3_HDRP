using BaseCore;
using UnityEngine;

namespace Interactable
{
    public class DungeonEntranceInteractable : InteractableObject
    {

        protected override void Interact()
        {
            
        }
        
        protected override void Enter()
        {
            
        }
        
        protected override void Exit()
        {
            
        }
    }
}
