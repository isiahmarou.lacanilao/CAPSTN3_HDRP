using Character;
using Character.CharacterComponents;
using DG.Tweening;
using ScriptableObjectData.CharacterData;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class PartyHPManaInfoPanel : MonoBehaviour
{
    public TextMeshProUGUI hpText;
    public Image hpBar;
    public TextMeshProUGUI manaText;
    public Image manaBar;

}
