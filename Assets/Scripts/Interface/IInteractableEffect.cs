public interface IInteractableEffect
{
    public void OnInteract();
    public void OnEnter();
    public void OnExit();
}