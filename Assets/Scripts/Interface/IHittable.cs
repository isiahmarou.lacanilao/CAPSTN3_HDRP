﻿namespace Interface
{
    public interface IHittable
    {
        public void Hit();
    }
}